import { FormBuilder, Validators } from '@angular/forms';

const fb = new FormBuilder();
export const createContactForm = fb.group({
  name: [
    '',
    [
      Validators.required,
      Validators.minLength(5),
      Validators.maxLength(10),
      Validators.pattern('[a-z | A-Z]*')
    ]
  ],
  lastName: [
    '',
    [
      Validators.required,
      Validators.minLength(3),
      Validators.maxLength(10),
      Validators.pattern('[a-z | A-Z]*')
    ]
  ],
  id: ['', [Validators.required, Validators.pattern('[0-9]*')]],
  email: ['', [Validators.required, Validators.email]],
  phone: ['', [Validators.pattern('[0-9 | ( | ) | -]*')]]
});
