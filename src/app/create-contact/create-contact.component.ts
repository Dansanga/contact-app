import { Component, OnInit, OnDestroy } from '@angular/core';
import { ContactListService } from '../core/services/contact-list.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { createContactForm } from './create-contact.forms';

@Component({
  selector: 'app-create-contact',
  templateUrl: './create-contact.component.html',
  styleUrls: ['./create-contact.component.scss']
})
export class CreateContactComponent implements OnInit, OnDestroy {
  private userForm = createContactForm;
  private createContactSubscription = new Subscription();

  constructor(
    private contactListService: ContactListService,
    private router: Router
  ) {}

  ngOnInit() {}

  ngOnDestroy() {
    this.createContactSubscription.unsubscribe();
  }

  private addContactHandler(): void {
    this.createContactSubscription = this.contactListService
      .createContact(this.userForm.value)
      .subscribe(() => {
        this.router.navigate(['/contacts']);
      });
  }

  private cancelContactHandler(): void {
    this.router.navigate(['/contacts']);
  }
}
