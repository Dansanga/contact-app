import { Component, OnInit, OnDestroy } from '@angular/core';
import { Contact } from '../core/models/contact';
import { ContactListService } from '../core/services/contact-list.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.scss']
})
export class ContactListComponent implements OnInit, OnDestroy {
  public contacts: Contact[];
  public counter = 3;
  private getContactSubscription = new Subscription();
  private deleteContactSubscription = new Subscription();

  constructor(
    private contactListService: ContactListService,
    private router: Router
  ) {}

  ngOnInit() {
    this.refreshContacts();
  }

  ngOnDestroy() {
    this.getContactSubscription.unsubscribe();
    this.deleteContactSubscription.unsubscribe();
  }

  private refreshContacts(): void {
    this.getContactSubscription = this.contactListService
      .getContacts()
      .subscribe(response => {
        this.contacts = response;
      });
  }

  private addContactHandler(): void {
    this.router.navigate(['/contact']);
  }

  private deleteContactHandler(id): void {
    this.deleteContactSubscription = this.contactListService
      .deleteContact(id)
      .subscribe(() => {
        this.refreshContacts();
      });
  }
}
