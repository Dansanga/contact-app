import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Contact } from '../../core/models/contact';

@Component({
  selector: 'app-contact-item',
  templateUrl: './contact-item.component.html',
  styleUrls: ['./contact-item.component.scss']
})
export class ContactItemComponent implements OnInit {
  @Input() contact: Contact;
  @Output() delete = new EventEmitter<string>();

  public constructor() {}

  public ngOnInit() {}

  doDelete() {
    this.delete.emit(<string>this.contact.id);
  }
}
