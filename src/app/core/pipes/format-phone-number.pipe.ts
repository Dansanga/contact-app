import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'formatPhoneNumber'
})
export class FormatPhoneNumberPipe implements PipeTransform {
  public transform(value: string, args?: any): any {
    let count = 0;
    return value
      ? value.split('').reduceRight((result, item, index) => {
          count++;
          if (count === 5) {
            result = item + '-' + result;
          } else if (count === 9) {
            result = item + ') ' + result;
            if (index === 0) {
              result = '(' + result;
            }
          } else if (count === 11 || (index > 9 && index < 11 && index === 0)) {
            result = ' (' + item + result;
          } else if (count === 12) {
            result = item + ' ' + result;
            if (index === 0) {
              result = '+' + result;
            }
          } else if (count > 12 && index === 0) {
            result = '+' + result;
          } else {
            result = item + result;
          }

          return result;
        }, '')
      : value;
  }
}
