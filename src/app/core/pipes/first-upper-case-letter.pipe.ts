import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'firstUpperCaseLetter'
})
export class FirstUpperCaseLetterPipe implements PipeTransform {
  public transform(value: string, args?: any): any {
    return value
      ? value.substr(0, 1).toUpperCase() +
          value.substr(1, value.length).toLowerCase()
      : value;
  }
}
