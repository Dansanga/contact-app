import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Contact } from '../models/contact';

@Injectable({
  providedIn: 'root'
})
export class ContactListService {
  private url = 'http://localhost:3000/contacts';

  constructor(private http: HttpClient) {}

  public getContacts<T>(): Observable<Contact[]> {
    return this.http.get<Contact[]>(this.url);
  }

  public getContact<T>(id: string): Observable<Contact> {
    return this.http.get<Contact>(this.url, { params: { id: id } });
  }

  public createContact<T>(contact: any): Observable<Contact[]> {
    return this.http.post<Contact[]>(this.url, contact);
  }

  public deleteContact<T>(id: string): Observable<Contact[]> {
    return this.http.delete<Contact[]>(`${this.url}/${id}`);
  }
}
