import { Component, OnInit, OnDestroy } from '@angular/core';
import { ContactListService } from '../core/services/contact-list.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Contact } from '../core/models/contact';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-contact-details',
  templateUrl: './contact-details.component.html',
  styleUrls: ['./contact-details.component.scss']
})
export class ContactDetailsComponent implements OnInit, OnDestroy {
  private contact: Contact;
  constructor(
    private contactListService: ContactListService,
    private route: ActivatedRoute,
    private router: Router
  ) {}
  private getContactSubscription = new Subscription();
  private routeMapSubscription = new Subscription();
  private deleteContactSubscription = new Subscription();

  ngOnInit() {
    this.routeMapSubscription = this.route.paramMap.subscribe(params => {
      this.getContactSubscription = this.contactListService
        .getContact(params.get('id'))
        .subscribe(response => {
          this.contact = response[0];
        });
    });
  }

  ngOnDestroy() {
    this.getContactSubscription.unsubscribe();
    this.routeMapSubscription.unsubscribe();
    this.deleteContactSubscription.unsubscribe();
  }

  private returnHandler(): void {
    this.router.navigate(['/contacts']);
  }

  private deleteContactHandler(contact): void {
    this.deleteContactSubscription = this.contactListService
      .deleteContact(contact.id)
      .subscribe(() => {
        this.router.navigate(['/contacts']);
      });
  }
}
